<?php  
require('includes/config.php');
require('includes/conexion.class.php');
require('includes/funciones.php');
require('includes/calendario.class.php');

//Si recibo un POST con la fecha, la agrego a la BBDD
if(isset($_POST['guardar'])){
  $fecha=$_POST['fecha'];
  $texto=$_POST['texto'];

  $trozosFecha=explode('/', $fecha);
  $fechaDia=$trozosFecha[0];
  $fechaMes=$trozosFecha[1];
  $fechaAnyo=$trozosFecha[2];

  $fechaTS=mktime(0,0,0,$fechaMes, $fechaDia, $fechaAnyo);

  $conexion=Conexion::conectar();
  $sql="INSERT INTO eventos (fechaEvento, textoEvento)VALUES($fechaTS, '$texto')";
  $conexion->query($sql);
}


$cal=new Calendario(2018);
//echo $cal->dibujaMes();
//echo $cal->dibujaAnyo();

if(isset($_GET['mes'])){
	$mes=$_GET['mes'];
}else{
	$mes=date('n'); //Genera el numero de mes actual SIN ceros
}

?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Bootstrap 101 Template</title>

    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-theme.min.css" rel="stylesheet">

  </head>
  <body>
  	<section class="container">
  		<h1>Calendario desde mi clase Calendario de PHP</h1>
  		<hr>
  			<?php echo $cal->dibujaMes($mes); ?>
  		<hr>
  		<a href="index.php?mes=<?php echo ($mes-1) ?>">Anterior</a>
  		<a href="index.php?mes=<?php echo ($mes+1) ?>">Siguiente</a>
      <hr>
      

      <!-- Trigger the modal with a button -->
     <!--  <button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal">Open Modal</button> -->

      <!-- Modal -->
      <div class="modal fade" id="myModal" role="dialog">
        <div class="modal-dialog">
        
          <!-- Modal content-->
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal">&times;</button>
              <h4 class="modal-title">Crear un evento</h4>
            </div>
            <div class="modal-body">
              
              <form action="index.php" method="post">
                <input type="hidden" value="" name="fecha" id="fecha">
                <br>
                <input type="text" name="texto" placeholder="Escribe el evento">
                <br>
                <input type="submit" name="guardar" value="Guardar">
              </form>

            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
            </div>
          </div>
          
        </div>
      </div>




      

  	</section>


    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>

    <script type="text/javascript">
      
      function dimeFecha(dia, mes, anyo){
        var txtFecha=dia+'/'+mes+'/'+anyo;
        $('#fecha').val(txtFecha);

      }

    </script>
  </body>
</html>




