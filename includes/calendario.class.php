<?php  
//Clase Calendario, para dibujar calendarios completos desde PHP, en HTML
// $c=new Calendario(1500);
class Calendario{

	private $anyo;
	private $conexion;

	public function __construct($an=false){
		if($an==false){
			$this->anyo=date('Y');
		}else{
			$this->anyo=$an;
		}
		$this->conexion=Conexion::conectar();
	}

	public function dibujaMes($mes=0){
		if($mes==0){
			$mes=date('n');
		}else if(($mes<1) OR ($mes>12)){
			$mes=date('n');
		}

		$meses=['', 'enero', 'febrero', 'marzo', 'abril', 'mayo', 'junio', 'julio', 'agosto', 'septiembre', 'octubre', 'noviembre', 'diciembre'];

		$r='<table border="1" class="table table-striped table-hover">';
		$r.='<tr><td colspan="7">'.$meses[$mes].' - '.$this->anyo.'</td></tr>';
		$r.='<tr><td>L</td><td>M</td><td>X</td><td>J</td><td>V</td><td>S</td><td>D</td></tr>';

		$fechaInicioMes=mktime(0,0,0,$mes, 1, $this->anyo);
		$inicioSemana=date('w', $fechaInicioMes);
		if($inicioSemana==0){
			$inicioSemana=7;
		}
		if($inicioSemana>1){
			$r.='<tr>';
			for($i=1;$i<$inicioSemana;$i++){
				$r.='<td></td>';
			}
		}

		//Bucle principal, para dibujar los dias del mes
		for($dia=1;$dia<=date('t', $fechaInicioMes); $dia++){

			$hoy=mktime(0,0,0,$mes, $dia, $this->anyo);

			//Si estamos a lunes
			if(date('w', $hoy)==1){
				$r.='<tr>';
			}

			//Dibujamos el dia
			//Aqui podemos PREGUNTAR DIFERENTES COSAS:
			$diaHoy=date('j');
			$mesHoy=date('n');
			$anyoHoy=date('Y');


			//EN este punto, tengo que preguntar, que eventos tengo en la bbdd
			$finicio=mktime(0,0,0,$mes,$dia,$this->anyo);
			$ffin=mktime(23,59,59,$mes,$dia,$this->anyo);
			$sql="SELECT * FROM eventos WHERE fechaEvento BETWEEN $finicio AND $ffin";
			$consulta=$this->conexion->query($sql);
			
			if($consulta->num_rows>0){
				$badge='<span class="badge">'.$consulta->num_rows.'</span>';
			}else{
				$badge='';
			}

			$conectado=true;

			if($conectado){
				$r.='<td data-toggle="modal" data-target="#myModal" onClick="dimeFecha('.$dia.', '.$mes.', '.$this->anyo.');">'.$dia.' '.$badge.'</td>';
			}else{
				$r.='<td>'.$dia.' '.$badge.'</td>';
			}


			//Si estamos a domingo
			if(date('w', $hoy)==0){
				$r.='</tr>';
			}
		} //Fin del bucle principal de los dias

		$r.='</table>';
		return $r;
	}

	public function dibujaAnyo(){
		$r='';
		$r.='<table border="1" class="table">';
		for($mes=1;$mes<=12;$mes++){
			//Puede que este al pricicipio de fila
			if($mes%3==1){
				$r.='<tr>';
			}

			//Dibuja el mes
			$r.='<td>'.$this->dibujaMes($mes).'</td>';

			//Puede que estemos al final de la final 
			if($mes%3==0){
				$r.='</tr>';
			}
		}
		$r.='</table>';
		return $r;
	}

}
?>