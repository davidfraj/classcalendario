<?php  
//Fichero con funciones utiles
//Funcion para cargar un modulo en mi pagina
function cargarModulo($modulo){
	require('modulos/'.$modulo.'/controllers/'.$modulo.'Controller.php');
}

//Funcion para transformar fecha de formato YYYY-MM-DD a TIMESTAMP
function fechaToTimestamp($fecha){
	// $trozos=explode('-', $fecha); 
	// $dia=$trozos[2];
	// $mes=$trozos[1];
	// $anyo=$trozos[0];
	// $lafecha=mktime(0,0,0,$mes, $dia, $anyo);
	// return $lafecha;

	$date = new DateTime($fecha);
	return $date->getTimestamp();	

}

//Funcion para mostrar una fecha como seres humanos
function timestampToFecha($timestamp){
	return date('d/m/Y', $timestamp);
}

?>